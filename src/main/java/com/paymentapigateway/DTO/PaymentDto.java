package com.paymentapigateway.DTO;


import lombok.Data;
import lombok.NoArgsConstructor;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class PaymentDto {


    private String paymentType;


    private BigDecimal amount;


    private String currency;


    private String creditorIban;


    private String debtorIban;

    private String details;


    private String creditorBIC;

}
