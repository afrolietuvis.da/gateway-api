package com.paymentapigateway.auth;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;
import java.util.Optional;
import java.util.Set;
import static com.paymentapigateway.security.UserRole.*;

@Repository("fake")
public class FakeApplicationUserDaoService implements UserDao{

    private final PasswordEncoder passwordEncoder;

    public FakeApplicationUserDaoService(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public Optional<ApplicationUser> selectApplicationUserByUsername(String username) {
        return getApplicationUsers().
                stream().
                filter(applicationUser -> username.equals(applicationUser.getUsername())).
                findFirst();
    }

    private Set<ApplicationUser> getApplicationUsers(){
        Set<ApplicationUser> applicationUsers = Set.of(
                new ApplicationUser(
                        "user",
                        passwordEncoder.encode("password"),
                        USER.getGrantedAuthorities(),
                        true,
                        true,
                        true,
                        true
                ),
                new ApplicationUser(
                        "admin",
                        passwordEncoder.encode("password"),
                        ADMIN.getGrantedAuthorities(),
                        true,
                        true,
                        true,
                        true
                )
        );
        return applicationUsers;
    }
}
