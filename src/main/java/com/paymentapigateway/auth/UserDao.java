package com.paymentapigateway.auth;

import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserDao{
    Optional<ApplicationUser> selectApplicationUserByUsername(String username);
}