package com.paymentapigateway.auth;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserService implements UserDetailsService {

    private final UserDao repository;

    public UserService(@Qualifier("fake") UserDao repository) {
        this.repository = repository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return this.repository.selectApplicationUserByUsername(username).
                orElseThrow(() ->
                        new UsernameNotFoundException(String.format("username %s not found", username)));
    }
}