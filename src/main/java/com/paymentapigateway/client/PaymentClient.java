package com.paymentapigateway.client;

import com.paymentapigateway.config.PaymentClientProperties;
import com.paymentapigateway.model.Payment;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;

@RestController
@RequestMapping("/payments")
public class PaymentClient {

    private final PaymentClientProperties props;
    private final RestTemplate restTemplate;

    public PaymentClient(PaymentClientProperties props, RestTemplateBuilder builder) {
        this.props = props;
        this.restTemplate = builder.
                setConnectTimeout(Duration.ofMillis(3000)).
                setReadTimeout(Duration.ofMillis(3000)).
                build();
    }

    @GetMapping
    @PreAuthorize("hasAuthority('payment:read')")
    Payment[] getPayments(){
        return restTemplate.getForObject("http://localhost:8082/payments", Payment[].class);
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('payment:read')")
    Payment getPayment(@PathVariable Long id){
        return restTemplate.getForObject("http://localhost:8082/payments/"+id, Payment.class);
    }





}
