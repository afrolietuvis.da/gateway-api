package com.paymentapigateway.client;

import com.paymentapigateway.config.RequestClientProperties;
import com.paymentapigateway.model.Payment;
import com.paymentapigateway.model.Request;
import lombok.extern.java.Log;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;
import java.util.List;

@Log
@RestController
@RequestMapping("/requests")
public class RequestClient {

    private final RequestClientProperties props;
    private final RestTemplate restTemplate;

    public RequestClient(RequestClientProperties props, RestTemplateBuilder builder) {
        this.props = props;
        this.restTemplate = builder.
                setConnectTimeout(Duration.ofMillis(3000)).
                setReadTimeout(Duration.ofMillis(3000)).
                build();
    }

//    @GetMapping
//    Payment[] getPayments() {
//        return restTemplate.getForObject(props.getUrl() + props.getBasePath(), Payment[].class);
//    }
//
//    @GetMapping("/{id}")
//    Payment getPayment(@PathVariable Long id){
//        return restTemplate.getForObject(props.getUrl()+props.getBasePath()+id, Payment.class);
//    }
    @PostMapping
    @ResponseBody
    @PreAuthorize("hasAuthority('request:write')")
    void createRequest(@RequestBody Request request){
        try {
            restTemplate.postForObject("http://localhost:8081/requests", request, Request.class);
        } catch (Exception e){
            log.warning(e.getMessage());
        }
    }
    @GetMapping
    @ResponseBody
    @PreAuthorize("hasAuthority('request:read')")
    Request[] getRequests(){
       return restTemplate.getForObject("http://localhost:8081/requests", Request[].class);
    }
}