package com.paymentapigateway.config;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix="request")
@Data
public class RequestClientProperties {

    private String url;
    private String basePath;
}
