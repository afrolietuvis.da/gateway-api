package com.paymentapigateway.model;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class Payment {

    private long id;

    private BigDecimal amount;

    private Currency currency;

    private PaymentType paymentType;

    private String debtorIban;

    private String creditorIban;

    private String creditorBIC;

    private BigDecimal feeOnCancel;

    private PaymentStatus paymentStatus;

    private String details;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyy-MM-dd HH:mm")
    private LocalDateTime createdOn;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyy-MM-dd HH:mm")
    private LocalDateTime canceledOn;


}
