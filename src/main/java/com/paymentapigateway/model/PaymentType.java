package com.paymentapigateway.model;

import java.math.BigDecimal;

public enum PaymentType {
    TYPE1(BigDecimal.valueOf(0.05)),
    TYPE2(BigDecimal.valueOf(0.1)),
    TYPE3(BigDecimal.valueOf(0.2));

    private final BigDecimal feeOnCancel;

    PaymentType(BigDecimal feeOnCancel) {
        this.feeOnCancel = feeOnCancel;
    }

    public BigDecimal getFeeOnCancel() {
        return feeOnCancel;
    }
}
