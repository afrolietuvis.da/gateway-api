package com.paymentapigateway.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
public class Request {

    private Long id;

    private BigDecimal amount;

    private String currency;

    private String paymentType;

    private String debtorIban;

    private String creditorIban;

    private String creditorBIC;

    private String details;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyy-MM-dd HH:mm")
    private LocalDateTime createdOn;

}
