package com.paymentapigateway.security;

import com.paymentapigateway.auth.UserService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import java.util.concurrent.TimeUnit;


@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final PasswordEncoder passwordEncoder;
    private final UserService userService;

    public SecurityConfig(PasswordEncoder passwordEncoder, UserService userService) {
        this.passwordEncoder = passwordEncoder;
        this.userService = userService;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.
        csrf().disable().
                authorizeRequests().
                antMatchers("/", "index", "/static/css/*", "/js/*").permitAll().
                anyRequest().authenticated().
                and().
                formLogin().
                loginPage("/login").permitAll().
                passwordParameter("password").
                usernameParameter("username").
                and().
                rememberMe().
                rememberMeParameter("remember-me").
                tokenValiditySeconds((int) TimeUnit.DAYS.toSeconds(21)).
                key("somethingverysecure").
                and().
                logout().
                logoutUrl("/logout").
                logoutRequestMatcher(new AntPathRequestMatcher("/logout", "GET")).
                clearAuthentication(true).
                invalidateHttpSession(true).
                deleteCookies("remember-me", "Idea-3d2c3533", "JSESSIONID", "XSRF-TOKEN").
                logoutSuccessUrl("/login").and().httpBasic();
    }
    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(daoAuthenticationProvider());
    }

    @Bean
    public DaoAuthenticationProvider daoAuthenticationProvider() {
        DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
        provider.setPasswordEncoder(passwordEncoder);
        provider.setUserDetailsService(userService);
        return provider;
    }
}
