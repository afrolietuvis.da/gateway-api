package com.paymentapigateway.security;

public enum UserPermission {

    PAYMENT_READ("payment:read"),
    PAYMENT_WRITE("payment:write"),
    REQUEST_READ("request:read"),
    REQUEST_WRITE("request:write");

    private final String permission;

    UserPermission(String permission) {
        this.permission = permission;
    }
    public String getPermission(){
        return permission;
    }
}
