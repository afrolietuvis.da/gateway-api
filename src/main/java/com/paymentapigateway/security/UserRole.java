package com.paymentapigateway.security;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import static com.paymentapigateway.security.UserPermission.*;

import java.util.Set;
import java.util.stream.Collectors;

public enum UserRole {
    USER(Set.of(
            REQUEST_READ,
            REQUEST_WRITE
    )),
    ADMIN(Set.of(
            PAYMENT_READ,
            PAYMENT_WRITE,
            REQUEST_READ,
            REQUEST_WRITE
    ));

    private final Set<UserPermission> permissions;

    UserRole(Set<UserPermission> permissions) {
        this.permissions = permissions;
    }

    public Set<UserPermission> getPermissions() {
        return permissions;
    }

    public Set<SimpleGrantedAuthority> getGrantedAuthorities(){
        Set<SimpleGrantedAuthority> permissions =
                getPermissions().
                        stream().
                        map(permission -> new SimpleGrantedAuthority(permission.getPermission())).
                        collect(Collectors.toSet());
        permissions.add(new SimpleGrantedAuthority("ROLE_" + this.name()));
        return permissions;
    }
}
